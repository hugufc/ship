# Instructions

## Setup

### Install Node

https://github.com/nodesource/distributions#debian-and-ubuntu-based-distributions

### Install MongoDB

https://docs.mongodb.com/manual/administration/install-on-linux/

### Install node modules on project folder

```bash
cd /path/to/project/
npm i
```

## How to run

### Development

```bash
npm run dev
```

### Tests

```bash
npm test
```

### Production

```bash
npm start
```

## Endpoints

You can test the endpoints below using a local server or accessing this server deployed on http://46.101.254.48

###  Get ship info

```http
GET /sonda/info
```

### Reset ship info

```http
POST /sonda/iniciar
```

### Move ship

```http
POST /sonda/mover
Content-Type: application/json

{
    "commands": ["GE", "M", "M", "M", "GD", "M", "M"]
}
```