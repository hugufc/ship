const Server = require('./server');
const test = require('ava');

test.beforeEach(async t => {
    const server = Server.create();
    let options = {
        method: 'POST',
        url: '/sonda/iniciar'
    };
    await server.inject(options);
});

test('get ship info', async t => {
    const server = Server.create();

    let options = {
        method: 'GET',
        url: '/sonda/info'
    };

    let response = await server.inject(options);

    t.pass();
});

test('reset ship info', async t => {
    const server = Server.create();

    let options = {
        method: 'POST',
        url: '/sonda/iniciar'
    };

    let response = await server.inject(options);

    t.pass();
});

test('move ship', async t => {
    const server = Server.create();

    let options = {
        method: 'POST',
        url: '/sonda/mover',
        payload: {
            commands: ['GE', 'M', 'M', 'M', 'GD', 'M', 'M'],
        }
    };

    let response = await server.inject(options);

    t.is(response.statusCode, 200);
    t.is(response.statusMessage, 'OK');
    t.deepEqual(response.result, { x: 2, y: 3, direction: 'D', description: 'A sonda virou para esquerda, se moveu 3 casas no eixo y, virou para direita e se moveu 2 casas no eixo x.' });
});

test('fail cross universe boundaries', async t => {
    const server = Server.create();

    let options = {
        method: 'POST',
        url: '/sonda/mover',
        payload: {
            commands: ['GE', 'M', 'M', 'M', 'M', 'M'],
        }
    };

    let response = await server.inject(options);

    t.is(response.statusCode, 400);
    t.is(response.statusMessage, 'Bad Request');
    t.is(response.result.message, 'Um movimento inválido foi detectado, infelizmente a sonda ainda não possui a habilidade de ir muito longe.');
});