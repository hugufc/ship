const Hapi = require('hapi');
const Boom = require('boom');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/universe', { useNewUrlParser: true });
mongoose.model('Ship', new mongoose.Schema({
    x: Number,
    y: Number,
    direction: String
}));

const getAngle = (direction) => direction === 'C' ? 0 : direction === 'D' ? 90 : direction === 'B' ? 180 : 270;

const getAngleMod = (angle) => Math.abs((angle / 360) % 1);

const getAxis = (angle) => {

    const mod = getAngleMod(angle);

    return mod === 0 || mod === 0.5 ? 'y' : 'x';
};

const getDirection = (angle) => {

    const mod = getAngleMod(angle);

    return mod === 0 ? 'C' : mod === 0.25 ? 'D' : mod === 0.5 ? 'B' : 'E';
};

const create = () => {

    const server = Hapi.server({
        port: process.env.NODE_ENV === 'production' ? 80 : 3000,
        host: process.env.NODE_ENV === 'production' ? '0.0.0.0' : 'localhost'
    });

    server.route({
        method: 'GET',
        path: '/sonda/info',
        handler: async (request, h) => {

            const Ship = mongoose.model('Ship');

            const { x, y, direction } = await Ship.findOne().exec() || { x: 0, y: 0, direction: 'D' };

            return { x, y, direction };
        }
    });

    server.route({
        method: 'POST',
        path: '/sonda/iniciar',
        handler: async (request, h) => {

            const Ship = mongoose.model('Ship');

            await Ship.findOneAndDelete().exec();

            return 'Posições da sonda reiniciadas.';
        }
    });

    server.route({
        method: 'POST',
        path: '/sonda/mover',
        handler: async (request, h) => {

            const commands = request.payload.commands;

            const Ship = mongoose.model('Ship');

            const ship = await Ship.findOne().exec() || new Ship({ x: 0, y: 0, direction: 'D' });

            let descriptionSteps = [];
            let currentAngle = getAngle(ship.direction);
            let moveCount = 0;

            while (commands.length > 0) {
                const command = commands.shift();
                switch (command) {
                    case 'GE':
                        currentAngle -= 90;
                        descriptionSteps.push('virou para esquerda');
                        break;
                    case 'GD':
                        currentAngle += 90;
                        descriptionSteps.push('virou para direita');
                        break;
                    case 'M':
                        moveCount++;
                        break;
                    default:
                        break;
                }

                if (moveCount > 0 && command !== commands[0]) {
                    const axis = getAxis(currentAngle);

                    ship[axis] += moveCount;
                    descriptionSteps.push(`se moveu ${moveCount} casas no eixo ${axis}`);

                    moveCount = 0;
                }
            }

            if (ship.x < 0 || ship.x > 4 || ship.y < 0 || ship.y > 4) {
                return Boom.badRequest('Um movimento inválido foi detectado, infelizmente a sonda ainda não possui a habilidade de ir muito longe.');
            }

            ship.direction = getDirection(currentAngle);

            await ship.save();

            let lastDescription = descriptionSteps.pop();

            return {
                x: ship.x,
                y: ship.y,
                direction: ship.direction,
                description: `A sonda ${descriptionSteps.join(', ')} e ${lastDescription}.`
            };
        }
    });

    return server;
};
module.exports = { create };